package prologdsl

import scala.collection.immutable.Stream
import scala.collection.immutable.Map

import scala.collection.mutable.HashMap

object PrologDSL {
  def listToString[T](xs : List[T]) : String = xs match {
    case Nil       => ""
    case List(x)   => x.toString()
    case (x :: xs) => x.toString() + ", " + listToString(xs)
  }

  trait Term {
    override def toString(): String = this match {
      case Num(n)    => ""+n
      case Atom(a)   => ""+a
      case Var(n)    => n
      case App(f, a) => f + "(" + listToString(a) + ")"
    }
  }
  //Term types
  case class Num(value : Int) extends Term
  case class Atom(value : String) extends Term
  case class Var(value : String) extends Term
  case class App(functor : String, args : List[Term] ) extends Term
  //Term :- Term, Term, ..., Term.
  case class Rule(head : Term,  body : Goal)
  
  type Goal = List[Term] //conjunction
  type SymTable = HashMap[String, List[Rule]]
  type Subst = Tuple2[String, Term]
  type Equation = Tuple2[Term, Term]
  type Answer = Map[String, Term]

  var globalST : SymTable = HashMap()
  
  //Qualify a predicate name with its arity
  def qualName(t : Term) : String = t match {
    case Atom(n)   => n + "/0"
    case App(n, a) => n + "/" + a.length
    case o         => throw new IllegalArgumentException("Can't use "+o+" as rule head")
  }
  
  def addRule(r : Rule, st : SymTable) = {
    val name = qualName(r.head)
    st += (name -> (st get name match {
      case None     => List(r)
      case Some(rs) => rs :+ r
    }))
  }

  //Lazy filter for matching rules, keeping the substitutions which made the rule head unify with t
  def matching(t : Term, rules : List[Rule]) : Stream[Tuple2[Rule, Answer]] = rules match {
    case Nil => Stream.empty
    case (r :: rs) => unify2(t, r.head) match {
      case None     => matching(t, rs)
      case Some(ss) => fixRAns(t, r) #:: matching(t, rs)
    }
  }

  //Rename a term and rule consistently
  //Kill me
  def fixRAns(t : Term, ri : Rule) : Tuple2[Rule, Answer] = {
    val r = renameRule(ri)
    val subs = unify2(t, r.head) match {
      case Some(s) => s
      case None    => throw new IllegalStateException("renaming variables caused unification to fail")
    }
    return (r, subs)
  }

  def renameRule(r : Rule) : Rule = {
    copy_term(App("f1", List(r.head, App("f2", r.body)))) match {
      case App(f1, List(h, App(f2, b))) => Rule(h, b)
    }
  }

  //Keep applying all the subs from b into a until we reach a fixpoint
  def collapseInto(a : Answer)(b : Answer) : Answer = {
    val r = a.mapValues(applyAny(b, _))
    if (r == a)
      return r
    else collapseInto(r)(b)      
  }
  
  def search(st : SymTable)(g : Goal) : Stream[Answer] = withRenamedG(g, gs => gs.toStream.flatMap(searchImpl(st)))

  def searchImpl(st : SymTable)(t : Term) : Stream[Answer] = {
    val qn = qualName(t)
    (st get qn) match {
      case None     => { println("no matching def for "+qn); return Stream.empty; }
      case Some(rs) => matching(t, rs).flatMap(searchIterRule(st))
    }
  }

  def searchIterRule(st : SymTable)(rans : Tuple2[Rule, Answer]) : Stream[Answer] = rans match {
    case (Rule(_, rB), subs) => searchIter(st)(rB, subs)
  }

  def searchIter(st : SymTable)(goal : Goal, subs : Answer) : Stream[Answer] = goal match {
    case Nil     => Stream(collapseInto(subs)(subs))
    case List(t) => singleCase(st)(t, subs).map(collapseInto(subs))
    case t :: ts => {
      val as = singleCase(st)(t, subs)
      val bs = searchIter(st)(ts, subs)

      return as.flatMap(av => {
	val nextQuery = ts.map(t => av.foldLeft(t)(applySubst))
	val childAnswer = searchIter(st)(nextQuery, subs)
	childAnswer.map(collapseInto(av))
      }).map(collapseInto(subs))
    }
  }
  
  def singleCase(st : SymTable)(t : Term, subs : Answer) : Stream[Answer] = {
    return searchImpl(st)(subs.foldLeft(t)(applySubst))
  }

  def withRenamedG(g : Goal, f : Goal => Stream[Answer]) : Stream[Answer] = {
    var renames : HashMap[String, String] = HashMap()
    val nn = g.map(rnv(_, renames))
    val res = f(nn)
    val origNames = renames.map(x => x.swap)
    return res.map(y => y.map(x => x match {
	case (k, v) => (origNames get k) match {
	  case None => (k, v)
	    case Some(orig) => (orig, v)
	}
    }))
  }
    
  var gsCount : Int = 0
  def gensym : String = {
    val v = ""+gsCount;
    gsCount += 1;
    return v;
  }
  
  def copy_term(t : Term) : Term = {
    var renames : HashMap[String, String] = HashMap()
    return rnv(t, renames)
  }

  def rnv(t : Term, rns : HashMap[String, String]) : Term = t match {
    case Var(n) => (rns get n) match {
      case None => {
	val nn = "_" + n + "_" + gensym
	rns += (n -> nn)
	return Var(nn)
      }
      case Some(nn) => Var(nn)
    }
    case App(f, as) => App(f, as.map(rnv(_, rns)))
    case tt => tt
  }

  def applyAny(ans : Answer, t : Term) : Term = varMap(t, s => ans get s)
  
  def printAns(ans : Answer) : Unit = {
    if (ans == ans.empty)
      println("yes")
    else printSubs(ans.toList)
  }

  def printSubs(subs : List[Subst]) : Unit = {    
    for ((n, v) <- subs)
      if (!n.startsWith("_"))
	println(n + " = " + v)
  }
  
  //Apply a function to possibly change the value of all the variable-refs in a Term
  def varMap(t : Term, f : String => Option[Term]) : Term = t match {
    case Var(n) => f(n) match {
      case Some(nn) => nn
      case None => t
    }
    case App(h, a) => App(h, a.map(varMap(_, f)))
    case _ => t
  }
  
  //Apply a single substitution
  def applySubst(t : Term, sub : Subst) : Term = sub match {
    case (n, v) => varMap(t, s => if (n == s) Some(v) else None)
  }
  
  //Apply a substitution to an equation
  def subEq(s : Subst)(eqn : Equation) : Equation = eqn match {
    case (l, r) => (applySubst(l, s), applySubst(r, s))
  }

  def unify2(a : Term, b : Term) : Option[Answer] = unify(List((a,b)), Map())
  
  def occurs_check(vn : String, t : Term) : Boolean = t match {
    case Var(`vn`) => true
    case App(f, a) => a.exists(occurs_check(vn, _))
    case _ => false
  }

  def unify(equations : List[Equation], subs : Answer) : Option[Answer] = {
    if (equations == Nil)
      return Some(subs)
    
    //add a substitution and keep unifying
    def addSub(nsub : Subst) : Option[Answer] = unify(equations.map(subEq(nsub)), subs + nsub)
    def addSubOC(vn : String, t : Term) : Option[Answer] = {
      if (occurs_check(vn, t)) {
	println("!!! occurs_check !!!")
	println("unifying "+equations)
	readLine;
      }
      addSub((vn, t))
    }

    val ((x,y) :: eqs) = equations
    if (x == y)
      unify(eqs, subs)
    else (x,y) match {
      case (Var(xn),
	    Var(yn)) if (xn == yn) => Some(subs)
      
      case (App(xf, xa),
	    App(yf, ya)) if (xf == yf && xa.length == ya.length) => unify(xa.zip(ya) ::: eqs, subs)
      
      case (Var(xn), t) => addSubOC(xn, t)
      case (t, Var(yn)) => addSubOC(yn, t)
	  
      case _ => None
    }
  }
}
