import scala.util.parsing.combinator._

import prologdsl.{PrologDSL => P}

object PrologTest extends JavaTokenParsers {
  def unify2(a : P.Term, b : P.Term) = {
    println(a+" = "+b);
    P.unify2(a, b) match {
      case Some(ans) => P.printAns(ans)
      case None => println("no")
    }
    println("");
  }

  def printInteractively(s : Stream[P.Answer]) : Unit = s match {
    case Stream() => { println("no"); return }
    case Stream(r) => P.printAns(r)
    case r #:: rs => {
      P.printAns(r)
      print("more?")
      if (Console.readLine().isEmpty())
	printInteractively(rs)
    }
  }

  //Shorthand
  def Num(v : Int) : P.Num = { P.Num(v) }
  def Atom(v : String) : P.Atom = { P.Atom(v) }
  def Var(v : String) : P.Var = { P.Var(v) }
  def App(f : String, a : P.Term*) : P.App = { P.App(f, a.toList) }
  
  private def s_atom = ("""[a-z]\p{javaJavaIdentifierPart}*""".r)
  private def termlist: Parser[List[P.Term]] = rep1sep(p_term, ",")
  
  //^^ :: Parser[A] -> (A -> B) -> Parser[B]
  def p_atom : Parser[P.Atom] = s_atom ^^ Atom
  def p_var  : Parser[P.Var]  = ("""[A-Z]\p{javaJavaIdentifierPart}*""".r) ^^ Var
  def p_num  : Parser[P.Num]  = wholeNumber ^^ { x => Num(x.toInt) }
  def p_app  : Parser[P.App]  = s_atom ~ "(" ~ termlist ~ ")" ^^ {
    case f ~ "(" ~ args ~ ")" => P.App(f, args)
  }
  def p_term : Parser[P.Term] = p_app | p_atom | p_var | p_num 

  def p_fact: Parser[P.Rule] = p_term ~ "." ^^ {
    case t ~ "." => P.Rule(t, Nil)
  }
  def p_rule: Parser[P.Rule] = p_term ~ ":-" ~ termlist ~ "." ^^ {
    case h ~ z ~ b ~ zz => P.Rule(h, b)
  }
  def p_def : Parser[P.Rule] = p_rule | p_fact
  
  def main(args: Array[String]) {
    var nr : Int = 0
    for (s <- io.Source.fromFile("test.pl").getLines())
      if (!s.trim().isEmpty()) {
	P.addRule(parseAll(p_def, s).get, P.globalST)
	nr += 1
      }

    println("OK, read "+nr+" rules")

    print("? ")
    for (s <- io.Source.stdin.getLines()) {
      if (!s.trim().isEmpty())
	printInteractively(P.search(P.globalST)(List(parseAll(p_term, s).get)))
      print("? ")
    }
  }
}
